import { IncomingForm } from "formidable";
import { NextFunction, Request, Response } from "express";

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    interface Request {
      fields?: any;
      files: any;
      expressFormidable: {
        parsed: boolean;
      };
    }
  }
}

type FormidableEventAction = (
  req: Request,
  res: Response,
  next: NextFunction,
  ...params: string[]
) => void;

interface FormidableEvent {
  event: string;
  action: FormidableEventAction;
}

export function formidableMiddleware(
  opts = {},
  events: FormidableEvent[] = []
) {
  return (req: Request, res: Response, next: NextFunction) => {
    if (req.expressFormidable && req.expressFormidable.parsed) {
      next();
      return;
    }

    const form = new IncomingForm();
    Object.assign(form, opts);

    let manageOnError = false;
    if (events) {
      events.forEach(e => {
        manageOnError = manageOnError || e.event === "error";
        form.on(e.event, (...parameters) => {
          e.action(req, res, next, ...parameters);
        });
      });
    }

    if (!manageOnError) {
      form.on("error", err => {
        next(err);
      });
    }

    form.parse(req, (err, fields, files) => {
      if (err) {
        next(err);
        return;
      }

      Object.assign(req, {
        fields,
        files,
        expressFormidable: { parsed: true }
      });

      next();
    });
  };
}
