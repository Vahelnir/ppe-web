import { Column, Entity, ManyToOne, PrimaryColumn } from "typeorm";
import { Member } from "./Member";

@Entity()
export class MemberSubscription {
  @PrimaryColumn({ type: "uuid" })
  memberId!: string;

  @PrimaryColumn({ type: "timestamp" })
  subscribedAt!: Date;

  @Column({ type: "timestamp" })
  acceptedAt!: Date;

  @Column({ type: "timestamp" })
  endedAt!: Date;

  @ManyToOne(
    type => Member,
    member => member.subscriptions
  )
  member!: Member;
}
