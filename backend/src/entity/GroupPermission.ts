import { Column, Entity, ManyToOne, PrimaryColumn } from "typeorm";
import { Group } from "./Group";

@Entity()
export class GroupPermission {
  @Column()
  @PrimaryColumn()
  permission!: string;

  @Column({ type: "varchar", length: 36 })
  @PrimaryColumn()
  groupId!: string;

  @ManyToOne(
    type => Group,
    group => group.permissions
  )
  group!: Group;
}
