import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Post } from "./Post";
import { Profile } from "./Profile";
import { UserPermission } from "./UserPermission";
import { Member } from "./Member";

@Entity()
export class User {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column()
  email!: string;

  @Column()
  password!: string;

  @CreateDateColumn({ type: "timestamp" })
  createdAt!: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt!: Date;

  @OneToMany(
    (type) => Profile,
    (profile) => profile.user
  )
  profiles!: Profile[];

  @Column()
  mainProfileId!: string;

  @OneToOne(
    (type) => Profile,
    (profile) => profile.userMainProfile
  )
  @JoinColumn()
  mainProfile!: Profile;

  @OneToMany(
    (type) => UserPermission,
    (userPermission) => userPermission.user
  )
  permissions!: UserPermission[];
}
