import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { Club } from "./Club";
import { Member } from "./Member";

@Entity()
export class Post {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column()
  image!: string;

  @Column()
  title!: string;

  @Column({ type: "text" })
  content!: string;

  @CreateDateColumn({ type: "timestamp" })
  createdAt!: Date;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt!: Date;

  @Column("uuid")
  authorId!: string;

  @Column("uuid")
  clubId!: string;

  @ManyToOne(
    type => Member,
    member => member.posts
  )
  @JoinColumn()
  author!: Member;

  @ManyToOne(
    type => Club,
    club => club.posts
  )
  @JoinColumn()
  club!: Club;
}
