import {
  Brackets,
  Column,
  Entity,
  getRepository,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { Post } from "./Post";
import { Club } from "./Club";
import { Profile } from "./Profile";
import { Group } from "./Group";
import { MemberSubscription } from "./MemberSubscription";
import { GroupPermission } from "./GroupPermission";

@Entity()
export class Member {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column({ type: "uuid" })
  clubId!: string;

  @Column({ type: "uuid" })
  profileId!: string;

  @OneToMany(
    type => Post,
    post => post.author
  )
  posts!: Post[];

  @OneToMany(
    type => MemberSubscription,
    memberSubscription => memberSubscription.member
  )
  subscriptions!: MemberSubscription[];

  @ManyToOne(
    type => Club,
    club => club.members
  )
  club!: Club;

  @ManyToOne(
    type => Profile,
    profile => profile.members
  )
  profile!: Profile;

  @ManyToMany(type => Group)
  @JoinTable({ name: "member_group" })
  groups!: Group[];

  getAllGroupPermissions() {
    const groupPermissionRepository = getRepository(GroupPermission);
    return groupPermissionRepository
      .createQueryBuilder("gp")
      .distinct()
      .innerJoinAndSelect(Group, "g", "g.id = gp.groupId")
      .innerJoinAndSelect("member_group", "mg", "mg.groupId = g.id")
      .innerJoinAndSelect(Member, "m", "m.id = mg.memberId")
      .where("m.id = :memberId")
      .setParameter("memberId", this.id)
      .getMany();
  }

  async getAllPermissions() {
    const groupPermissions = await this.getAllGroupPermissions();
    return groupPermissions.map(groupPermission => groupPermission.permission);
  }

  hasPermission(permission: string) {
    return this.hasPermissions([permission]);
  }

  async hasPermissions(permissions: string[]) {
    const groupPermissionRepository = getRepository(GroupPermission);
    const query = groupPermissionRepository
      .createQueryBuilder("gp")
      .distinct()
      .select("gp.permission")
      .innerJoinAndSelect(Group, "g", "g.id = gp.groupId")
      .innerJoinAndSelect("member_group", "mg", "mg.groupId = g.id")
      .innerJoinAndSelect(Member, "m", "m.id = mg.memberId")
      .where("m.id = :memberId", { memberId: this.id })
      .andWhere("gp.permission IN (:...permissions)", {
        permissions: [...permissions, "*"]
      });
    const matchCount = await query.getCount();
    return matchCount > 0;
  }
}
