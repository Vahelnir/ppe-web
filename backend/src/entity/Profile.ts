import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn
} from "typeorm";
import { User } from "./User";
import { Member } from "./Member";

@Entity()
export class Profile {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column()
  email!: string;

  @Column()
  name!: string;

  @Column()
  surname!: string;

  @Column()
  address!: string;

  @Column()
  postcode!: string;

  @Column()
  city!: string;

  @Column()
  country!: string;

  @Column()
  gender!: string;

  @ManyToOne(
    type => User,
    user => user.profiles
  )
  @JoinColumn()
  user!: User;

  @Column({ type: "uuid" })
  userId!: string;

  @OneToOne(
    type => User,
    user => user.mainProfile
  )
  userMainProfile!: User;

  @OneToMany(
    type => Member,
    member => member.profile
  )
  members!: Member[];
}
