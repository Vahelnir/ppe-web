import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Post } from "./Post";
import { Group } from "./Group";
import { Member } from "./Member";

@Entity()
export class Club {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column()
  name!: string;

  @Column()
  icon!: string;

  @OneToMany(
    type => Post,
    post => post.club
  )
  posts!: Post[];

  @OneToMany(
    type => Group,
    group => group.club
  )
  groups!: Group[];

  @OneToMany(
    type => Member,
    member => member.club
  )
  members!: Member[];
}
