import { Column, Entity, ManyToOne, PrimaryColumn } from "typeorm";
import { User } from "./User";

@Entity()
export class UserPermission {
  @Column()
  @PrimaryColumn()
  permission!: string;

  @Column({ type: "varchar", length: 36 })
  @PrimaryColumn()
  userId!: string;

  @ManyToOne(
    type => User,
    user => user.permissions
  )
  user!: User;
}
