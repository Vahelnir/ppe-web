import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  JoinTable,
} from "typeorm";
import { GroupPermission } from "./GroupPermission";
import { Club } from "./Club";
import { Member } from "./Member";

@Entity()
export class Group {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column()
  label!: string;

  @Column()
  clubId!: string;

  @OneToMany(
    (type) => GroupPermission,
    (groupPermission) => groupPermission.group
  )
  permissions!: GroupPermission[];

  @ManyToMany((type) => Member)
  @JoinTable({ name: "member_group" })
  members!: Member[];

  @ManyToOne(
    (type) => Club,
    (club) => club.posts
  )
  club!: Club;
}
