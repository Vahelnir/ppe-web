import { MigrationInterface, QueryRunner } from "typeorm";

export class addMemberSubscription1578670656614 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
        CREATE TABLE \`member_subscription\`
        (
            \`memberId\` varchar(36) NOT NULL,
            \`subscribedAt\` datetime NOT NULL,
            \`acceptedAt\` datetime DEFAULT NULL,
            \`endedAt\` datetime DEFAULT NULL
        );
    `);
    await queryRunner.query(
      `ALTER TABLE \`member_subscription\`
                ADD PRIMARY KEY ( \`memberId\`, \`subscribedAt\`),
                ADD CONSTRAINT \`member_subscription\` FOREIGN KEY (\`memberId\`) REFERENCES \`member\` (\`id\`);`
    );
    await queryRunner.query(`
        ALTER TABLE \`member\`
            DROP \`subscribedAt\`,
            DROP \`acceptedAt\`,
            DROP \`endedAt\`;
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
