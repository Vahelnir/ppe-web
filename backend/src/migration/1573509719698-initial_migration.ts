import { MigrationInterface, QueryRunner } from "typeorm";

export class initialMigration1573509719698 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE \`club\` (
        \`id\` varchar(36) PRIMARY KEY,
        \`name\` varchar(255) UNIQUE NOT NULL,
        \`icon\` varchar(255),
        \`description\` varchar(255)
      );
    `);
    await queryRunner.query(`
      CREATE TABLE \`post\` (
        \`id\` varchar(36) PRIMARY KEY,
        \`image\` varchar(255) NOT NULL,
        \`title\` varchar(255) NOT NULL,
        \`content\` text NOT NULL,
        \`createdAt\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        \`updatedAt\` datetime,
        \`authorId\` varchar(36) NOT NULL,
        \`clubId\` varchar(36) NOT NULL
      );
    `);
    await queryRunner.query(`
      CREATE TABLE \`profile\` (
        \`id\` varchar(36) PRIMARY KEY,
        \`email\` varchar(255) NOT NULL,
        \`name\` varchar(255) NOT NULL,
        \`surname\` varchar(255) NOT NULL,
        \`address\` varchar(255) NOT NULL,
        \`postcode\` varchar(255) NOT NULL,
        \`city\` varchar(255) NOT NULL,
        \`country\` varchar(255) NOT NULL,
        \`gender\` int(11) NOT NULL,
        \`userId\` varchar(36) NOT NULL
      );
    `);
    await queryRunner.query(`
      CREATE TABLE \`user\` (
        \`id\` varchar(36) PRIMARY KEY,
        \`email\` varchar(255) NOT NULL,
        \`password\` varchar(255) NOT NULL,
        \`createdAt\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        \`updatedAt\` datetime,
        \`mainProfileId\` varchar(36)
      );
    `);
    await queryRunner.query(`
      CREATE TABLE \`group_permission\` (
        \`groupId\` varchar(36),
        \`permission\` varchar(255),
        PRIMARY KEY (\`groupId\`, \`permission\`)
      );
      `);
    await queryRunner.query(`
      CREATE TABLE \`member_group\` (
        \`groupId\` varchar(36),
        \`memberId\` varchar(36),
        PRIMARY KEY (\`groupId\`, \`memberId\`)
      );
    `);
    await queryRunner.query(`
      CREATE TABLE \`member\` (
        \`id\` varchar(36) PRIMARY KEY,
        \`clubId\` varchar(36),
        \`profileId\` varchar(36),
        \`subscribedAt\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        \`acceptedAt\` datetime,
        \`endedAt\` datetime
      );
    `);
    await queryRunner.query(`
      CREATE TABLE \`group\` (
        \`id\` varchar(36) PRIMARY KEY,
        \`label\` varchar(255),
        \`clubId\` varchar(36)
      );
    `);
    await queryRunner.query(`
      CREATE TABLE \`user_permission\` (
        \`userId\` varchar(36),
        \`permission\` varchar(255),
        PRIMARY KEY (\`userId\`, \`permission\`)
      );
    `);
    await queryRunner.query(
      `ALTER TABLE \`post\` ADD CONSTRAINT \`post_author\` FOREIGN KEY (\`authorId\`) REFERENCES \`member\` (\`id\`);`
    );
    await queryRunner.query(
      `ALTER TABLE \`profile\` ADD CONSTRAINT \`profile_user\` FOREIGN KEY (\`userId\`) REFERENCES \`user\` (\`id\`);`
    );
    await queryRunner.query(
      `ALTER TABLE \`post\` ADD CONSTRAINT \`post_club\` FOREIGN KEY (\`clubId\`) REFERENCES \`club\` (\`id\`);`
    );
    await queryRunner.query(
      `ALTER TABLE \`user\` ADD CONSTRAINT \`user_main_profile\` FOREIGN KEY (\`mainProfileId\`) REFERENCES \`profile\` (\`id\`);`
    );
    await queryRunner.query(
      `ALTER TABLE \`user_permission\` ADD CONSTRAINT \`user_permission_user\` FOREIGN KEY (\`userId\`) REFERENCES \`user\` (\`id\`);`
    );
    await queryRunner.query(
      `ALTER TABLE \`group_permission\` ADD CONSTRAINT \`group_permission_group\` FOREIGN KEY (\`groupId\`) REFERENCES \`group\` (\`id\`);`
    );
    await queryRunner.query(
      `ALTER TABLE \`group\` ADD CONSTRAINT \`group_club\` FOREIGN KEY (\`clubId\`) REFERENCES \`club\` (\`id\`);`
    );
    await queryRunner.query(
      `ALTER TABLE \`member\` ADD CONSTRAINT \`member_profile\` FOREIGN KEY (\`profileId\`) REFERENCES \`profile\` (\`id\`);`
    );
    await queryRunner.query(
      `ALTER TABLE \`member\` ADD CONSTRAINT \`member_club\` FOREIGN KEY (\`clubId\`) REFERENCES \`club\` (\`id\`);`
    );
    await queryRunner.query(
      `ALTER TABLE \`member_group\` ADD CONSTRAINT \`member_group_member\` FOREIGN KEY (\`memberId\`) REFERENCES \`member\` (\`id\`);`
    );
    await queryRunner.query(
      `ALTER TABLE \`member_group\` ADD CONSTRAINT \`member_group_group\` FOREIGN KEY (\`groupId\`) REFERENCES \`group\` (\`id\`);`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await this.dropForeignKeyAndTables(queryRunner, [
      "post",
      "profile",
      "user",
      "group_permission",
      "member_group",
      "group",
      "user_permission",
      "club"
    ]);
  }

  private async dropForeignKeyAndTables(
    queryRunner: QueryRunner,
    names: string[]
  ) {
    const tables = await queryRunner.getTables(names);
    for (const table of tables) {
      await queryRunner.dropForeignKeys(table, table.foreignKeys);
    }
    for (const table of tables) {
      await queryRunner.dropTable(table);
    }
  }
}
