import "reflect-metadata";
import { createConnection, getRepository } from "typeorm";
import express, { Request } from "express";
import { ApolloServer } from "apollo-server-express";
import { join } from "path";
import { fileLoader, mergeTypes } from "merge-graphql-schemas";
import session from "express-session";
import fileStoreBuilder from "session-file-store";
import bodyParser from "body-parser";
import { formidableMiddleware } from "./middleware/formidable";
import { RenameDirective } from "./graphql/directive/RenameDirective";
import { resolvers } from "./graphql/resolver";
import { login, logout, register } from "./routes/auth";
import { User } from "./entity/User";
import { Profile } from "./entity/Profile";
import { GraphqlContext } from "./graphql/GraphQLContext";

const FileStore = fileStoreBuilder(session);

createConnection()
  .then(() => {
    const mergedSchemas = fileLoader(join(__dirname, "../schema/**/*.graphql"));
    const typeDefs = mergeTypes(mergedSchemas, { all: true });

    const apollo = new ApolloServer({
      typeDefs,
      rootValue: {},
      resolvers,
      schemaDirectives: {
        rename: RenameDirective
      },
      async context({ req }): Promise<GraphqlContext> {
        const { userId, selectedProfileId } = req.session || {
          userId: undefined,
          selectedProfileId: null
        };
        const user = userId
          ? await getRepository(User).findOne(userId)
          : undefined;
        const selectedProfile = selectedProfileId
          ? await getRepository(Profile).findOne(selectedProfileId)
          : undefined;
        return {
          user,
          selectedProfile,
          request: req,
          session: req.session || {}
        };
      },
      playground: process.env.NODE_ENV !== "production",
      introspection: true
    });
    const server = express();
    server.use(bodyParser.urlencoded({ extended: false }));
    server.use(bodyParser.json());
    server.use(
      session({
        store: new FileStore({
          secret:
            "IQdEZVWN8VWJHl014dVYYmhWfvW7FMDws13YF7iHKXt0iAio9090tQpXrnrFbxF"
        }),
        secret:
          "CjMVjhKPADt6tiQ8s1OWKEZrKS2Bbt6M6wsa2ChClPVKA5ijtR5y9IYA3NFI8Ei",
        cookie: {
          httpOnly: true,
          sameSite: "lax"
        },
        name: "m2l_session",
        resave: true,
        saveUninitialized: true
      })
    );
    apollo.applyMiddleware({ app: server, path: "/api/graphql" });

    server.use("/api/", formidableMiddleware());
    server.post("/api/auth/login", login);
    server.post("/api/auth/register", register);
    server.post("/api/auth/logout", logout);
    server.listen(process.env.PPE_PORT || 8080, () => {
      console.log(`Listening on http://localhost:8080${apollo.graphqlPath}`);
    });
  })
  .catch(error => console.error(error));
