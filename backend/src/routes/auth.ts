import { Request, Response } from "express";
import { argon2id, hash, verify } from "argon2";
import { getRepository } from "typeorm";
import { User } from "../entity/User";

const EMAIL_PASSWORD_NOT_FOUND = {
  connected: false,
  error: {
    id: "EMAIL_PASSWORD_NOT_FOUND",
    text: "There is no user with that email and password"
  }
};

export async function login(req: Request, res: Response) {
  const { email, password } = req.fields;
  const userRepository = getRepository(User);
  const user = await userRepository.findOne({ where: { email } });
  if (!user) {
    return res.status(401).json(EMAIL_PASSWORD_NOT_FOUND);
  }
  let matches = false;
  try {
    matches = await verify(user.password, password);
  } catch (err) {
    return res.status(401).json({
      connected: false,
      error: {
        id: "UNKNOWN_ERROR",
        text: "unknown error"
      }
    });
  }
  if (!matches) {
    return res.status(401).json(EMAIL_PASSWORD_NOT_FOUND);
  }
  if (!req.session) {
    return res.status(500).json({
      connected: false,
      error: {
        id: "INTERNAL_SERVER_ERROR",
        text: ""
      }
    });
  }
  req.session.userId = user.id;
  return res.json({
    connected: true
  });
}

export async function register(req: Request, res: Response) {
  if (!req.session) {
    return res.status(500).json({
      connected: false,
      error: {
        id: "INTERNAL_SERVER_ERROR",
        text: ""
      }
    });
  }
  if (req.session.userId) {
    return res.status(400).json({
      created: false,
      connected: false,
      error: {
        id: "ALREADY_LOGGED_IN",
        text: "You are already logged in"
      }
    });
  }
  const { email, password, verificationPassword } = req.fields;
  const userRepository = getRepository(User);
  let user = await userRepository.findOne({ where: { email } });
  if (user) {
    return res.status(400).json({
      created: false,
      connected: false,
      error: {
        id: "EMAIL_ALREADY_USED",
        text: "An account with this email already exists"
      }
    });
  }
  if (password !== verificationPassword) {
    return res.status(400).json({
      created: false,
      connected: false,
      error: {
        id: "PASSWORD_NOT_MATCHING",
        text: "The two passwords do not match"
      }
    });
  }
  let hashed: string;
  try {
    hashed = await hash(password, {
      type: argon2id
    });
  } catch (err) {
    return res.status(400).json({
      error: {
        created: false,
        connected: false,
        id: "UNKNOWN_ERROR",
        text: "unknown error"
      }
    });
  }
  user = new User();
  user.email = email;
  user.password = hashed;
  await userRepository.save(user);
  req.session.userId = user.id;
  return res.json({ created: true, connected: true });
}

export async function logout(req: Request, res: Response) {
  if (!req.session) {
    return res.json(true);
  }
  delete req.session.userId;
  delete req.session.selectedProfileId;
  return res.json(true);
}
