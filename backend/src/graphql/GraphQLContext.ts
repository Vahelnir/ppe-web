import { User } from "../entity/User";
import { Profile } from "../entity/Profile";
import { Request } from "express";

export interface GraphqlContext {
  user: User | undefined;
  selectedProfile: Profile | undefined;
  request: Request;
  session: {
    [key: string]: any;
  };
}
