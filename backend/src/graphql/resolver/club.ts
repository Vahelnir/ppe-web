import { getRepository } from "typeorm";
import { Club } from "../../entity/Club";
import { Post } from "../../entity/Post";
import { Group } from "../../entity/Group";
import { MemberSubscription } from "../../entity/MemberSubscription";
import { Member } from "../../entity/Member";
import { GraphqlContext } from "../GraphQLContext";
import { UserPermission } from "../../entity/UserPermission";
import { GraphQLError } from "graphql";
import { GroupPermission } from "../../entity/GroupPermission";
import { Profile } from "../../entity/Profile";

export const clubResolver = {
  Club: {
    posts(parent: Club) {
      return getRepository(Post).find({ where: { clubId: parent.id } });
    },
    async groups(parent: Club) {
      return getRepository(Group).find({ where: { clubId: parent.id } });
    },
    members(parent: Club) {
      return getRepository(Member).find({ where: { clubId: parent.id } });
    },
  },
  Query: {
    club(parent: undefined, { id, name }: { id: string; name: string }) {
      const clubRepository = getRepository(Club);
      const where: any = {};
      if (id) {
        where.id = id;
      }
      if (name) {
        where.name = name;
      }
      return clubRepository.findOne({ where });
    },
    clubs() {
      const clubRepository = getRepository(Club);
      return clubRepository.find();
    },
  },
  Mutation: {
    async createClub(
      parent: undefined,
      { name, icon }: { name: string; icon: string | null },
      { user }: GraphqlContext
    ) {
      if (!user) {
        throw new GraphQLError("Should be logged !");
      }
      const userPermissionRepository = getRepository(UserPermission);
      const currentUserPermissions = await userPermissionRepository.find({
        where: { userId: user.id },
      });
      if (!currentUserPermissions.some((perm) => perm.permission === "admin")) {
        throw new GraphQLError("You are not an administrator");
      }
      const clubRepository = getRepository(Club);
      let club = new Club();
      club.name = name;
      club.icon = icon ? icon : "";
      club = await clubRepository.save(club);

      const groupRepository = getRepository(Group);
      let group = new Group();
      group.label = "Gestionnaire";
      group.clubId = club.id;
      group = await groupRepository.save(group);

      const groupPermissionRepository = getRepository(GroupPermission);
      const permission = new GroupPermission();
      permission.permission = "*";
      permission.groupId = group.id;
      await groupPermissionRepository.save(permission);
      return clubRepository.findOne({ where: { id: club.id } });
    },
    async removeClub(
      parent: undefined,
      { id }: { id: string },
      { user }: GraphqlContext
    ) {
      if (!user) {
        throw new GraphQLError("Should be logged !");
      }
      const userPermissionRepository = getRepository(UserPermission);
      const currentUserPermissions = await userPermissionRepository.find({
        where: { userId: user.id },
      });
      if (!currentUserPermissions.some((perm) => perm.permission === "admin")) {
        throw new GraphQLError("You are not an administrator");
      }
      const clubRepository = getRepository(Club);
      const club = await clubRepository.findOne({ where: { id } });
      if (!club) {
        throw new GraphQLError("No club with that id");
      }

      const memberRepository = getRepository(Member);
      const members = await memberRepository.find({ where: { clubId: id } });
      for (const member of members) {
        const memberSubscriptionRepository = getRepository(MemberSubscription);
        const subs = await memberSubscriptionRepository.find({
          where: { memberId: member.id },
        });
        memberSubscriptionRepository.remove(subs);
      }
      await memberRepository.remove(members);

      const groupRepository = getRepository(Group);
      const groups = await groupRepository.find({ where: { clubId: id } });

      for (const group of groups) {
        const groupPermissionRepository = getRepository(GroupPermission);
        const groupPermissions = await groupPermissionRepository.find({
          where: { groupId: group.id },
        });
        await groupPermissionRepository.remove(groupPermissions);
      }
      await groupRepository.remove(groups);
      await clubRepository.remove(club);
      return true;
    },
    async addProfileWithGroup(
      parent: undefined,
      { profileId, groupId }: { profileId: string; groupId: string },
      { user }: GraphqlContext
    ) {
      if (!user) {
        throw new GraphQLError("Should be logged !");
      }
      const userPermissionRepository = getRepository(UserPermission);
      const currentUserPermissions = await userPermissionRepository.find({
        where: { userId: user.id },
      });
      if (!currentUserPermissions.some((perm) => perm.permission === "admin")) {
        throw new GraphQLError("You are not an administrator");
      }

      const profileRepository = getRepository(Profile);
      const profile = await profileRepository.findOne({
        where: { id: profileId },
      });
      if (!profile) {
        throw new GraphQLError("no profile with that id !");
      }

      const groupRepository = getRepository(Group);
      const group = await groupRepository.findOne({
        where: { id: groupId },
      });
      if (!group) {
        throw new GraphQLError("No group with that id !");
      }

      const memberRepository = getRepository(Member);
      let member = await memberRepository.findOne({
        where: { profileId: profile.id, clubId: group.clubId },
        relations: ["groups"],
      });
      if (!member) {
        const newMember = new Member();
        newMember.clubId = group.clubId;
        newMember.profileId = profile.id;
        member = await memberRepository.save(newMember);

        const now = new Date();
        const future = new Date();
        future.setFullYear(now.getFullYear() + 1);
        console.log(now);
        const subRepository = getRepository(MemberSubscription);
        let sub = new MemberSubscription();
        sub.memberId = member.id;
        sub.subscribedAt = now;
        sub.acceptedAt = now;
        sub.endedAt = future;
        sub.memberId = member.id;

        sub = await subRepository.save(sub);
        member = await memberRepository.findOne({
          where: { profileId: profile.id, clubId: group.clubId },
          relations: ["groups"],
        });
        if (!member) {
          throw new GraphQLError(
            "an error occured while creating a new member"
          );
        }
      }

      const groupNotPresent = !member.groups.some((gr) => gr.id === group.id);
      if (groupNotPresent) {
        member.groups.push(group);
        await memberRepository.save(member);
      }
      return true;
    },
  },
};
