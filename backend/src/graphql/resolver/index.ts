import { merge } from "lodash";
import { clubResolver } from "./club";
import { postResolver } from "./post";
import { queryResolver } from "./query";
import { userResolver } from "./user";
import { profileResolver } from "./profile";
import { memberResolver } from "./member";
import { groupResolver } from "./group";

export const resolvers = merge(
  {},
  clubResolver,
  postResolver,
  queryResolver,
  userResolver,
  profileResolver,
  memberResolver,
  groupResolver
);
