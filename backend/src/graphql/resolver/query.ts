import { GraphqlContext } from "../GraphQLContext";

export const queryResolver = {
  Query: {
    async me(parent: undefined, args: undefined, context: GraphqlContext) {
      if (context.user && context.user.id) {
        return context.user;
      }
      return null;
    },
    selectedProfile(
      parent: undefined,
      args: undefined,
      context: GraphqlContext
    ) {
      if (context.user) {
        return context.selectedProfile;
      }
      return null;
    }
  }
};
