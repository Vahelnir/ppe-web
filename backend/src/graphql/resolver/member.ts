import { getRepository } from "typeorm";
import { AuthenticationError, UserInputError } from "apollo-server-express";
import { MemberSubscription } from "../../entity/MemberSubscription";
import { Member } from "../../entity/Member";
import { Club } from "../../entity/Club";
import { Post } from "../../entity/Post";
import { Profile } from "../../entity/Profile";
import { ForbiddenError } from "apollo-server";
import { GraphqlContext } from "../GraphQLContext";

const unsubscribedStatus = { status: "unsubscribed", subscription: null };

function checkSubscription(subscription: MemberSubscription | undefined) {
  if (!subscription) {
    return unsubscribedStatus;
  }

  if (!subscription.acceptedAt) {
    return { status: "pending", subscription: subscription };
  }

  if (subscription.acceptedAt.getTime() === subscription.endedAt.getTime()) {
    return { status: "refused", subscription: subscription };
  }

  const now = new Date();
  if (subscription.acceptedAt <= now && subscription.endedAt > now) {
    return { status: "subscribed", subscription: subscription };
  }
  if (subscription.endedAt <= now) {
    return { status: "expired", subscription: subscription };
  }
  return unsubscribedStatus;
}

async function getSubscriptionStatus(profileId: string, clubId: string) {
  const memberSubscriptionRepository = getRepository(MemberSubscription);
  const memberRepository = getRepository(Member);
  const member = await memberRepository.findOne({
    select: ["id"],
    where: { profileId, clubId },
  });
  if (!member) {
    return unsubscribedStatus;
  }
  const subscription = await memberSubscriptionRepository.findOne({
    where: { memberId: member.id },
    order: { subscribedAt: "DESC" },
  });
  return checkSubscription(subscription);
}

export const memberResolver = {
  SubscriptionStatus: {
    PENDING: "pending",
    SUBSCRIBED: "subscribed",
    EXPIRED: "expired",
    REFUSED: "refused",
    UNSUBSCRIBED: "unsubscribed",
    NOTLOGGED: "notlogged",
  },
  Member: {
    club(parent: Member) {
      return getRepository(Club).findOne({ where: { id: parent.clubId } });
    },
    posts(parent: Member) {
      return getRepository(Post).find({ where: { authorId: parent.id } });
    },
    profile(parent: Member) {
      return getRepository(Profile).findOne({
        where: { id: parent.profileId },
      });
    },
    async groups(parent: Member) {
      const member = await getRepository(Member).findOne({
        where: { id: parent.id },
        relations: ["groups"],
      });
      return member?.groups;
    },
    subscriptions(parent: Member) {
      return getRepository(MemberSubscription).find({
        where: { id: parent.clubId },
      });
    },
    async status(parent: Member) {
      const memberSubscriptionRepository = getRepository(MemberSubscription);
      const subscription = await memberSubscriptionRepository.findOne({
        where: { memberId: parent.id },
        order: { subscribedAt: "DESC" },
      });
      console.log(new Date());
      const res = checkSubscription(subscription);
      console.log(subscription, res);
      return res;
    },
  },
  Club: {
    async subscribed(
      parent: Club,
      _args: unknown,
      { session }: GraphqlContext
    ) {
      const selectedProfileId = session.selectedProfileId;
      if (!selectedProfileId) return { status: "notlogged" };
      return getSubscriptionStatus(selectedProfileId, parent.id);
    },
  },
  Profile: {
    async subscribedTo(
      parent: Profile,
      { clubName: name }: { clubName: string }
    ) {
      const clubRepository = getRepository(Club);
      const club = await clubRepository.findOne({
        select: ["id"],
        where: { name },
      });
      if (!club) {
        throw new UserInputError("no club with that name");
      }
      return getSubscriptionStatus(parent.id, club.id);
    },
  },
  Mutation: {
    async subscribeToClub(
      parent: undefined,
      { clubName }: { clubName: string },
      { selectedProfile }: GraphqlContext
    ) {
      if (!selectedProfile) {
        throw new AuthenticationError("No profile selected");
      }
      const clubRepository = getRepository(Club);
      const memberRepository = getRepository(Member);

      const club = await clubRepository.findOne({ where: { name: clubName } });
      if (!club) {
        throw new UserInputError("no club with that name");
      }
      let member = await memberRepository.findOne({
        where: { profileId: selectedProfile.id, clubId: club.id },
      });
      if (!member) {
        const newMember = new Member();
        newMember.clubId = club.id;
        newMember.profileId = selectedProfile.id;
        member = await memberRepository.save(newMember);
      }
      // TODO: check if there is an active subscription. If so then return its status
      const subscription = new MemberSubscription();
      subscription.memberId = member.id;
      subscription.subscribedAt = new Date();
      const subscriptionRepository = getRepository(MemberSubscription);
      const finalSubscription = await subscriptionRepository.save(subscription);
      return checkSubscription(finalSubscription);
    },
    async acceptSubscription(
      parent: undefined,
      { memberId }: { memberId: string },
      { selectedProfile }: GraphqlContext
    ) {
      if (!selectedProfile) {
        throw new ForbiddenError(
          "You are not logged in or have not selected a profile"
        );
      }
      const memberRepository = getRepository(Member);
      const member = await memberRepository.findOne({
        where: { id: memberId },
      });
      if (!member) {
        throw new UserInputError("no member with that id");
      }

      const connectedMember = await memberRepository.findOne({
        where: { profileId: selectedProfile.id, clubId: member.clubId },
      });
      if (!connectedMember) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const hasPermission = await connectedMember.hasPermission(
        "member.decline"
      );
      if (!hasPermission) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }

      const memberSubscriptionRepository = getRepository(MemberSubscription);
      let subscription = await memberSubscriptionRepository.findOne({
        where: { memberId: member.id },
        order: { subscribedAt: "DESC" },
      });
      if (subscription) {
        const now = new Date();
        subscription.acceptedAt = now;
        const nextYear = new Date();
        nextYear.setFullYear(now.getFullYear() + 1);
        subscription.endedAt = nextYear;
        subscription = await memberSubscriptionRepository.save(subscription);
      }
      return checkSubscription(subscription);
    },
    async declineSubscription(
      parent: undefined,
      { memberId }: { memberId: string },
      { selectedProfile }: GraphqlContext
    ) {
      if (!selectedProfile) {
        throw new ForbiddenError(
          "You are not logged in or have not selected a profile"
        );
      }
      const memberRepository = getRepository(Member);
      const member = await memberRepository.findOne({
        where: { id: memberId },
      });
      if (!member) {
        throw new UserInputError("no member with that id");
      }

      const connectedMember = await memberRepository.findOne({
        where: { profileId: selectedProfile.id, clubId: member.clubId },
      });
      if (!connectedMember) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const hasPermission = await connectedMember.hasPermission(
        "member.accept"
      );
      if (!hasPermission) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }

      const memberSubscriptionRepository = getRepository(MemberSubscription);
      let subscription = await memberSubscriptionRepository.findOne({
        where: { memberId: member.id },
        order: { subscribedAt: "DESC" },
      });
      if (subscription) {
        const now = new Date();
        if (!subscription.acceptedAt) {
          subscription.acceptedAt = now;
        }
        subscription.endedAt = now;
        subscription = await memberSubscriptionRepository.save(subscription);
      }
      return checkSubscription(subscription);
    },
  },
};
