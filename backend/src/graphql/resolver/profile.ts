import { getRepository } from "typeorm";
import { UserInputError, ApolloError } from "apollo-server";
import { Profile } from "../../entity/Profile";
import { Member } from "../../entity/Member";
import { Club } from "../../entity/Club";
import { User } from "../../entity/User";
import { GraphqlContext } from "../GraphQLContext";

export const profileResolver = {
  Gender: {
    MAN: 0,
    WOMAN: 1,
    OTHER: 2
  },
  Profile: {
    async member(parent: Profile, { clubName: name }: { clubName: string }) {
      const memberRepository = getRepository(Member);
      const clubRepository = getRepository(Club);
      const club = await clubRepository.findOne({
        select: ["id"],
        where: { name }
      });
      if (!club) return null;
      return memberRepository.findOne({
        where: {
          profileId: parent.id,
          clubId: club.id
        }
      });
    },
    user(parent: Profile) {
      return getRepository(User).findOne({ where: { id: parent.userId } });
    },
    members(parent: Profile) {
      return getRepository(Member).find({ where: { profileId: parent.id } });
    }
  },
  Mutation: {
    async createProfile(
      parent: undefined,
      {
        profile: {
          email,
          name,
          surname,
          address,
          postcode,
          city,
          country,
          gender
        }
      }: {
        profile: {
          email: string;
          name: string;
          surname: string;
          address: string;
          postcode: string;
          city: string;
          country: string;
          gender: string;
        };
      },
      { session }: GraphqlContext
    ) {
      const userId = session.userId;
      const user = await getRepository(User).findOne({ where: { id: userId } });
      if (!user) {
        throw new ApolloError("An error occured");
      }
      const profileRepository = getRepository(Profile);
      let profile = await profileRepository.findOne({ where: { email } });
      if (profile) {
        throw new UserInputError(
          "Cet email est déjà utilisé par un autre profil"
        );
      }
      profile = new Profile();
      profile.email = email;
      profile.name = name;
      profile.surname = surname;
      profile.address = address;
      profile.postcode = postcode;
      profile.city = city;
      profile.country = country;
      profile.gender = gender;
      profile.userId = userId;
      profile = await profileRepository.save(profile);
      if (!user.mainProfileId) {
        user.mainProfileId = profile.id;
      }
      return profile;
    },
    async editProfile(
      parent: undefined,
      {
        id,
        profile: {
          email,
          name,
          surname,
          address,
          postcode,
          city,
          country,
          gender
        }
      }: {
        id: string;
        profile: {
          email: string;
          name: string;
          surname: string;
          address: string;
          postcode: string;
          city: string;
          country: string;
          gender: string;
        };
      },
      { session }: GraphqlContext
    ) {
      const userId = session.userId;
      const profileRepository = getRepository(Profile);
      const profile = await profileRepository.findOne({
        where: { id, userId }
      });
      if (!profile) {
        throw new UserInputError("Une erreur est survenue.");
      }
      profile.email = email;
      profile.name = name;
      profile.surname = surname;
      profile.address = address;
      profile.postcode = postcode;
      profile.city = city;
      profile.country = country;
      profile.gender = gender;
      return await profileRepository.save(profile);
    },
    async setActiveProfile(
      parent: undefined,
      { id }: { id: string },
      { session }: GraphqlContext
    ) {
      const userId = session.userId;
      const profileRepository = getRepository(Profile);
      const profile = await profileRepository.findOne(id);
      if (profile && profile.userId === userId) {
        session.selectedProfileId = profile.id;
        return profile;
      }
      return null;
    }
  },
  User: {
    profiles(parent: User) {
      return getRepository(Profile).find({ where: { userId: parent.id } });
    },
    profile(parent: User, { id }: { id: string }) {
      return getRepository(Profile).findOne({
        where: { userId: parent.id, id }
      });
    }
  }
};
