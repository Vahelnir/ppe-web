import { getRepository } from "typeorm";
import { UserInputError, ForbiddenError } from "apollo-server";
import { Post } from "../../entity/Post";
import { Club } from "../../entity/Club";
import { Member } from "../../entity/Member";
import { GraphqlContext } from "../GraphQLContext";

export const postResolver = {
  Query: {
    post(parent: undefined, { id }: { id: string }) {
      const postRepository = getRepository(Post);
      return postRepository.findOne({
        where: { id }
      });
    },
    posts() {
      const clubRepository = getRepository(Post);
      return clubRepository.find({
        order: {
          createdAt: "DESC"
        }
      });
    }
  },
  Mutation: {
    async createClubPost(
      parent: undefined,
      {
        clubName,
        postInput
      }: {
        clubName: string;
        postInput: { title: string; image: string; content: string };
      },
      { selectedProfile }: GraphqlContext
    ) {
      if (!selectedProfile) {
        throw new ForbiddenError(
          "You are not logged in or have not selected a profile"
        );
      }
      const clubRepository = getRepository(Club);
      const club = await clubRepository.findOne({ where: { name: clubName } });
      if (!club) {
        throw new UserInputError("no club with that name");
      }

      const memberRepository = getRepository(Member);
      const member = await memberRepository.findOne({
        where: { profileId: selectedProfile.id, clubId: club.id }
      });
      if (!member) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const hasPermission = await member.hasPermission("post.create");
      if (!hasPermission) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }

      const postRepository = getRepository(Post);
      const { title, image, content } = postInput;
      const post = new Post();
      post.title = title;
      post.image = image;
      post.content = content;
      post.clubId = club.id;
      post.authorId = member.id;
      return await postRepository.save(post);
    },
    async editClubPost(
      parent: undefined,
      {
        postId,
        postInput
      }: {
        postId: string;
        postInput: { title: string; image: string; content: string };
      },
      { selectedProfile }: GraphqlContext
    ) {
      if (!selectedProfile) {
        throw new ForbiddenError(
          "You are not logged in or have not selected a profile"
        );
      }
      // get post
      const postRepository = getRepository(Post);
      const post = await postRepository.findOne({ where: { id: postId } });
      if (!post) {
        throw new UserInputError("no post with that id.");
      }

      // get post's club
      const clubRepository = getRepository(Club);
      const club = await clubRepository.findOne({ where: { id: post.clubId } });
      if (!club) {
        throw new UserInputError(
          "this post has no club, you should contact an administrator."
        );
      }

      // get post's club member of current selected profile
      const memberRepository = getRepository(Member);
      const member = await memberRepository.findOne({
        where: { profileId: selectedProfile.id, clubId: club.id }
      });
      if (!member) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const hasPermission = await member.hasPermission("post.edit");
      if (!hasPermission) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const { title, image, content } = postInput;
      post.title = title;
      post.image = image;
      post.content = content;
      return await postRepository.save(post);
    },
    async removeClubPost(
      parent: undefined,
      { postId }: { postId: string },
      { selectedProfile }: GraphqlContext
    ) {
      if (!selectedProfile) {
        throw new ForbiddenError(
          "You are not logged in or have not selected a profile"
        );
      }
      // get post
      const postRepository = getRepository(Post);
      const post = await postRepository.findOne({ where: { id: postId } });
      if (!post) {
        throw new UserInputError("no post with that id.");
      }

      // get post's club
      const clubRepository = getRepository(Club);
      const club = await clubRepository.findOne({ where: { id: post.clubId } });
      if (!club) {
        throw new UserInputError(
          "this post has no club, you should contact an administrator."
        );
      }

      // get post's club member of current selected profile
      const memberRepository = getRepository(Member);
      const member = await memberRepository.findOne({
        where: { profileId: selectedProfile.id, clubId: club.id }
      });
      if (!member) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const hasPermission = await member.hasPermission("post.remove");
      if (!hasPermission) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const deleteResult = await postRepository.delete(postId);
      return deleteResult.affected && deleteResult.affected > 0;
    }
  },
  Post: {
    author(parent: Post) {
      return getRepository(Member).findOne({ where: { id: parent.authorId } });
    },
    club(parent: Post) {
      return getRepository(Club).findOne({ where: { id: parent.clubId } });
    }
  },
  Club: {
    posts(parent: Club) {
      const clubRepository = getRepository(Post);
      return clubRepository.find({
        where: {
          clubId: parent.id
        },
        order: {
          createdAt: "DESC"
        }
      });
    }
  }
};
