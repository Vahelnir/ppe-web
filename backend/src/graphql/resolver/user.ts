import { getRepository } from "typeorm";
import { Profile } from "../../entity/Profile";
import { User } from "../../entity/User";
import { UserPermission } from "../../entity/UserPermission";

export const userResolver = {
  User: {
    async permissions(parent: User) {
      const userPermissions = await getRepository(UserPermission).find({
        where: { userId: parent.id },
      });
      return userPermissions.map((userPerm) => userPerm.permission);
    },
  },
  Query: {
    users() {
      return getRepository(User).find();
    },
    user(parent: undefined, { id, email }: { id: string; email: string }) {
      const where: { id?: string; email?: string } = {};
      if (id) {
        where.id = id;
      }
      if (email) {
        where.email = email;
      }
      return getRepository(User).findOne({ where });
    },
  },
};
