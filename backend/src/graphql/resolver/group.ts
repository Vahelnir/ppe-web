import { getRepository } from "typeorm";
import { Member } from "../../entity/Member";
import {
  AuthenticationError,
  ForbiddenError,
  UserInputError,
  ApolloError
} from "apollo-server-express";
import { Club } from "../../entity/Club";
import { Group } from "../../entity/Group";
import { GroupPermission } from "../../entity/GroupPermission";
import { GraphqlContext } from "../GraphQLContext";

export const groupResolver = {
  Group: {
    async permissions(parent: Group) {
      const perms = await getRepository(GroupPermission).find({
        where: { groupId: parent.id }
      });
      return perms.map(perm => perm.permission);
    },
    async members(parent: Group) {
      const query = getRepository(Member)
        .createQueryBuilder("m")
        .leftJoin("member_group", "mg", "mg.memberId = m.id")
        .where("mg.groupId = :groupId", { groupId: parent.id });
      return query.getMany();
    }
  },
  Mutation: {
    async createGroup(
      parent: undefined,
      params: {
        clubName: string;
        group: { label: string; permissions: string[] };
      },
      { selectedProfile }: GraphqlContext
    ) {
      const {
        clubName,
        group: { label, permissions }
      } = params;
      if (!selectedProfile) {
        throw new AuthenticationError("No profile selected");
      }
      const clubRepository = getRepository(Club);
      const club = await clubRepository.findOne({ where: { name: clubName } });
      if (!club) {
        throw new UserInputError("no club with that name");
      }

      const memberRepository = getRepository(Member);
      const member = await memberRepository.findOne({
        where: { profileId: selectedProfile.id, clubId: club.id }
      });
      if (!member) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const hasPermission = await member.hasPermission("*");
      if (!hasPermission) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const groupRepository = getRepository(Group);
      let group = new Group();
      group.club = club;
      group.label = label;
      group = await groupRepository.save(group);
      const groupPermissions = permissions.map(permission => {
        const groupPermission = new GroupPermission();
        groupPermission.groupId = group.id;
        groupPermission.permission = permission;
        return groupPermission;
      });
      const groupPermissionRepository = getRepository(GroupPermission);
      await groupPermissionRepository.save(groupPermissions);
      return await groupRepository.save(group);
    },
    async editGroup(
      parent: undefined,
      params: {
        groupId: string;
        group: { label: string; permissions: string[] };
      },
      { selectedProfile }: GraphqlContext
    ) {
      const {
        groupId,
        group: { label, permissions }
      } = params;
      if (!selectedProfile) {
        throw new AuthenticationError("No profile selected");
      }
      const groupRepository = getRepository(Group);
      let group = await groupRepository.findOne({
        where: { id: groupId },
        relations: ["permissions", "club"]
      });
      if (!group) {
        throw new UserInputError("no group with that ID");
      }
      const club = group.club;

      const memberRepository = getRepository(Member);
      const member = await memberRepository.findOne({
        where: { profileId: selectedProfile.id, clubId: club.id }
      });
      if (!member) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const hasPermission = await member.hasPermission("*");
      if (!hasPermission) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const existingPermissions = [...group.permissions];
      delete group.permissions;
      group.label = label;
      group = await groupRepository.save(group);

      const groupPermissionRepository = getRepository(GroupPermission);
      await groupPermissionRepository.remove(existingPermissions);
      const groupPermissions = permissions.map(permission => {
        if (!group) {
          throw new ApolloError("an error occured");
        }
        const groupPermission = new GroupPermission();
        groupPermission.groupId = group.id;
        groupPermission.permission = permission;
        return groupPermission;
      });
      await groupPermissionRepository.save(groupPermissions);
      return groupRepository.findOne({ where: { id: group.id } });
    },
    async removeGroup(
      parent: undefined,
      { groupId }: { groupId: string },
      { selectedProfile }: GraphqlContext
    ) {},
    async addMemberToGroup(
      parent: undefined,
      { groupId, memberId }: { groupId: string; memberId: string },
      { selectedProfile }: GraphqlContext
    ) {
      if (!selectedProfile) {
        throw new AuthenticationError("No profile selected");
      }

      const selectedGroup = await getRepository(Group).findOne({
        where: { id: groupId },
        relations: ["club"]
      });
      if (!selectedGroup) {
        throw new AuthenticationError("no group with that ID");
      }

      const memberRepository = getRepository(Member);
      const loggedMember = await memberRepository.findOne({
        where: { profileId: selectedProfile.id, clubId: selectedGroup.club.id }
      });
      if (!loggedMember) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const hasPermission = await loggedMember.hasPermission("*");
      if (!hasPermission) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }

      const member = await memberRepository.findOne({
        where: { id: memberId },
        relations: ["groups"]
      });
      if (!member) {
        throw new AuthenticationError("no member with that ID");
      }
      if (member.clubId !== selectedGroup.club.id) {
        throw new AuthenticationError(
          "that member is not in the same club as the selected group"
        );
      }
      const groupNotPresent = !member.groups.find(
        group => group.id === selectedGroup.id
      );
      if (groupNotPresent) {
        member.groups.push(selectedGroup);
        await memberRepository.save(member);
      }
      return true;
    },
    async removeMemberFromGroup(
      parent: undefined,
      { groupId, memberId }: { groupId: string; memberId: string },
      { selectedProfile }: GraphqlContext
    ) {
      if (!selectedProfile) {
        throw new AuthenticationError("No profile selected");
      }

      const selectedGroup = await getRepository(Group).findOne({
        where: { id: groupId },
        relations: ["club"]
      });
      if (!selectedGroup) {
        throw new AuthenticationError("no group with that ID");
      }

      const memberRepository = getRepository(Member);
      const loggedMember = await memberRepository.findOne({
        where: { profileId: selectedProfile.id, clubId: selectedGroup.club.id }
      });
      if (!loggedMember) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }
      const hasPermission = await loggedMember.hasPermission("*");
      if (!hasPermission) {
        throw new ForbiddenError(
          "You do not have the required permission to perform this action"
        );
      }

      const member = await memberRepository.findOne({
        where: { id: memberId },
        relations: ["groups"]
      });
      if (!member) {
        throw new AuthenticationError("no member with that ID");
      }
      if (member.clubId !== selectedGroup.club.id) {
        throw new AuthenticationError(
          "that member is not in the same club as the selected group"
        );
      }
      const groupIsPresent = member.groups.some(
        group => group.id === selectedGroup.id
      );
      if (groupIsPresent) {
        member.groups = member.groups.filter(
          group => selectedGroup.id !== group.id
        );
        await memberRepository.save(member);
      }
      return true;
    }
  },
  Query: {
    group(parent: undefined, { groupId }: { groupId: string }) {
      return getRepository(Group).findOne({ where: { id: groupId } });
    }
  }
};
