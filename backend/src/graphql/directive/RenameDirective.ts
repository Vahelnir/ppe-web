import { SchemaDirectiveVisitor } from "graphql-tools";
import {
  defaultFieldResolver,
  GraphQLField,
  GraphQLResolveInfo
} from "graphql";
import { ExecutionContext } from "graphql/execution/execute";

export class RenameDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field: GraphQLField<any, any>) {
    const { name } = this.args;
    const { resolve = defaultFieldResolver } = field;
    field.resolve = async (
      object: any,
      args: any,
      context: ExecutionContext,
      info: GraphQLResolveInfo
    ) => {
      object[field.name] = object[name];
      return resolve.call(this, object, args, context, info);
    };
  }
}
