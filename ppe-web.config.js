module.exports = {
  apps: [
    {
      name: "ppe-api",
      cwd: "/srv/ppe.valentinhv.fr/backend",
      script: "npm",
      args: "run start",
      env: {
        NODE_ENV: "production",
        PPE_PORT: 8080
      }
    },
    {
      name: "ppe-static",
      cwd: "/srv/ppe.valentinhv.fr/frontend/dist/",
      script: "serve",
      env: {
        PM2_SERVE_PATH: ".",
        PM2_SERVE_PORT: 8081,
        PM2_SERVE_SPA: "true",
        PM2_SERVE_HOMEPAGE: "/index.html"
      }
    },
    {
      name: "ppe-docs",
      cwd: "/srv/ppe.valentinhv.fr/docs/",
      script: "serve",
      env: {
        PM2_SERVE_PATH: ".",
        PM2_SERVE_PORT: 8082
      }
    }
  ]
};
