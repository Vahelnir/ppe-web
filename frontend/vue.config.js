module.exports = {
  pluginOptions: {
    apollo: {
      lintGQL: false
    }
  },
  devServer: {
    proxy: {
      "/api": {
        target: "http://localhost:8080/",
        ws: true
      }
    }
  }
};
