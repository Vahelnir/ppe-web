module.exports = {
  client: {
    service: {
      name: "webapi",
      url: "http://localhost:8080/api/graphql",
      skipSSLValidation: true
    },
    addTypename: true,
    includes: ["src/**/*.{ts,tsx,vue,graphql}"]
  }
};
