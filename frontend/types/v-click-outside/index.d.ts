declare module "v-click-outside" {
  import { PluginObject } from "vue";
  const plugin: PluginObject<unknown>;

  export default plugin;
}
