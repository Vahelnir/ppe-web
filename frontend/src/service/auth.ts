export function login(email: string, password: string) {
  const form = new FormData();
  form.append("email", email);
  form.append("password", password);
  return fetch(window.BASE_URL + "/api/auth/login", {
    method: "POST",
    body: form
  });
}

export function register(
  email: string,
  password: string,
  verificationPassword: string
) {
  const form = new FormData();
  form.append("email", email);
  form.append("password", password);
  form.append("verificationPassword", verificationPassword);
  return fetch(window.BASE_URL + "/api/auth/register", {
    method: "POST",
    body: form
  });
}

export function disconnect() {
  return fetch(window.BASE_URL + "/api/auth/logout", {
    method: "POST"
  });
}
