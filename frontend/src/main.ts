import "typeface-open-sans";
import Vue from "vue";
import VueCompositionApi from "@vue/composition-api";
import App from "./App.vue";
import router from "./router";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
// @ts-ignore
import vSelect from "vue-select";
import vClickOutside from "v-click-outside";
import "./sportsIcons";

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component("v-select", vSelect);

Vue.use(VueCompositionApi);
Vue.use(vClickOutside);

Vue.config.productionTip = false;

declare global {
  interface Window {
    BASE_URL: string;
  }
}

window.BASE_URL = location.protocol + "//" + location.host;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
