import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { defaultDataIdFromObject, InMemoryCache } from "apollo-cache-inmemory";

const httpLink = createHttpLink({
  uri: location.protocol + "//" + location.host + "/api/graphql"
});

const cache = new InMemoryCache({
  dataIdFromObject(object: any) {
    if (object.__typename === "MemberSubscription") {
      object._id = object.memberId + ":" + object.subscribedAt;
    }
    return defaultDataIdFromObject(object);
  }
});

export const apolloClient = new ApolloClient({
  link: httpLink,
  cache
});
