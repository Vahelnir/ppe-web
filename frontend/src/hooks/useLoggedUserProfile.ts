import { useMutation, useQuery } from "@vue/apollo-composable";
import CREATE_USER_PROFILE from "@/graphql/profile/createUserProfile.graphql";
import EDIT_USER_PROFILE from "@/graphql/profile/editUserProfile.graphql";
import GET_USER_PROFILE from "@/graphql/profile/getUserProfile.graphql";
import GET_LOGGED_USER_PROFILE from "@/graphql/getLoggedUserProfile.graphql";
import {
  CreateUserProfile,
  CreateUserProfileVariables
} from "@/graphql/profile/types/CreateUserProfile";
import {
  EditUserProfile,
  EditUserProfileVariables
} from "@/graphql/profile/types/EditUserProfile";
import {
  GetUserProfile,
  GetUserProfileVariables
} from "@/graphql/profile/types/GetUserProfile";
import { GetLoggedUserProfile } from "@/graphql/types/GetLoggedUserProfile";

export function useUserCreateProfile() {
  return useMutation<CreateUserProfile, CreateUserProfileVariables | {}>(
    CREATE_USER_PROFILE,
    { variables: {}, errorPolicy: "all" }
  );
}

export function useUserEditProfile() {
  return useMutation<EditUserProfile, EditUserProfileVariables | {}>(
    EDIT_USER_PROFILE,
    {
      variables: {},
      errorPolicy: "all"
    }
  );
}

export function useUserProfile(variables: GetUserProfileVariables) {
  return useQuery<GetUserProfile, GetUserProfileVariables>(
    GET_USER_PROFILE,
    variables
  );
}

export function useLoggedProfile() {
  return useQuery<GetLoggedUserProfile>(GET_LOGGED_USER_PROFILE);
}
