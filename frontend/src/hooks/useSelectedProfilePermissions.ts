import { useQuery, useResult } from "@vue/apollo-composable";
import {
  GetLoggedMemberPermissionsForClub,
  GetLoggedMemberPermissionsForClubVariables
} from "@/graphql/types/GetLoggedMemberPermissionsForClub";
import GET_LOGGED_MEMBER_PERMISSIONS_FOR_CLUB from "@/graphql/getLoggedMemberPermissionsForClub.graphql";

export function useSelectedProfilePermissions(clubName: string) {
  const query = useQuery<
    GetLoggedMemberPermissionsForClub,
    GetLoggedMemberPermissionsForClubVariables
  >(GET_LOGGED_MEMBER_PERMISSIONS_FOR_CLUB, { clubName });
  const permissions = useResult(query.result, [] as string[], data =>
    data.selectedProfile?.member?.groups?.flatMap(group => group.permissions)
  );
  function hasPermission(...requiredPermissions: string[]) {
    requiredPermissions = [...requiredPermissions, "*"];
    if (permissions.value) {
      const perms = permissions.value;
      return requiredPermissions.some(perm => perms.includes(perm));
    }
    return false;
  }
  return { ...query, hasPermission, permissions };
}
