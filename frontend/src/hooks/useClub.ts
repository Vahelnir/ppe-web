import { useMutation, useQuery } from "@vue/apollo-composable";
import GET_CLUB_POSTS from "@/graphql/club/getClubPosts.graphql";
import GET_CLUB_POST from "@/graphql/club/getClubPost.graphql";
import EDIT_CLUB_POST from "@/graphql/club/editClubPost.graphql";
import CREATE_CLUB_POST from "@/graphql/club/createClubPost.graphql";
import REMOVE_CLUB_POST from "@/graphql/club/removeClubPost.graphql";
import GET_CLUB_MEMBERS from "@/graphql/club/getClubMembers.graphql";
import GET_CLUB_GROUP from "@/graphql/club/getClubGroup.graphql";
import GET_CLUB_GROUPS from "@/graphql/club/getClubGroups.graphql";
import CREATE_CLUB_GROUP from "@/graphql/club/createGroup.graphql";
import EDIT_CLUB_GROUP from "@/graphql/club/editGroup.graphql";
import GET_CLUB_GROUP_MEMBERS from "@/graphql/club/getClubGroupMembers.graphql";
import ADD_MEMBER_TO_GROUP from "@/graphql/club/addMemberToGroup.graphql";
import REMOVE_MEMBER_FROM_GROUP from "@/graphql/club/removeMemberFromGroup.graphql";
import {
  GetClubPosts,
  GetClubPostsVariables
} from "@/graphql/club/types/GetClubPosts";
import {
  GetClubPost,
  GetClubPostVariables
} from "@/graphql/club/types/GetClubPost";
import {
  EditClubPost,
  EditClubPostVariables
} from "@/graphql/club/types/EditClubPost";
import {
  CreateClubPost,
  CreateClubPostVariables
} from "@/graphql/club/types/CreateClubPost";
import {
  GetClubMembers,
  GetClubMembersVariables
} from "@/graphql/club/types/GetClubMembers";
import {
  RemoveClubPost,
  RemoveClubPostVariables
} from "@/graphql/club/types/RemoveClubPost";
import {
  AcceptClubSubscripton,
  AcceptClubSubscriptonVariables
} from "@/graphql/club/types/AcceptClubSubscripton";
import ACCEPT_SUBSCRIPTION from "@/graphql/club/acceptClubSubscription.graphql";
import {
  DeclineClubSubscripton,
  DeclineClubSubscriptonVariables
} from "@/graphql/club/types/DeclineClubSubscripton";
import DECLINE_SUBSCRIPTION from "@/graphql/club/declineClubSubscription.graphql";
import {
  CreateGroup,
  CreateGroupVariables
} from "@/graphql/club/types/CreateGroup";
import { EditGroup, EditGroupVariables } from "@/graphql/club/types/EditGroup";
import {
  GetClubGroups,
  GetClubGroupsVariables
} from "@/graphql/club/types/GetClubGroups";
import {
  GetClubGroup,
  GetClubGroupVariables
} from "@/graphql/club/types/GetClubGroup";
import {
  GetClubGroupMembers,
  GetClubGroupMembersVariables
} from "@/graphql/club/types/GetClubGroupMembers";
import {
  AddMemberToGroup,
  AddMemberToGroupVariables
} from "@/graphql/club/types/AddMemberToGroup";
import {
  RemoveMemberFromGroup,
  RemoveMemberFromGroupVariables
} from "@/graphql/club/types/RemoveMemberFromGroup";

export function useClubPosts(clubName: string) {
  return useQuery<GetClubPosts, GetClubPostsVariables>(GET_CLUB_POSTS, {
    name: clubName
  });
}

export function useClubPost(postId: string) {
  return useQuery<GetClubPost, GetClubPostVariables>(GET_CLUB_POST, {
    id: postId
  });
}

export function useEditClubPost(clubName: string) {
  return useMutation<EditClubPost, EditClubPostVariables | {}>(EDIT_CLUB_POST, {
    variables: {},
    refetchQueries: [
      {
        query: GET_CLUB_POSTS,
        variables: { name: clubName }
      }
    ]
  });
}

export function useCreateClubPost(clubName: string) {
  return useMutation<CreateClubPost, CreateClubPostVariables | {}>(
    CREATE_CLUB_POST,
    {
      variables: {},
      refetchQueries: [
        {
          query: GET_CLUB_POSTS,
          variables: { name: clubName }
        }
      ]
    }
  );
}

export function useRemoveClubPost(clubName: string) {
  return useMutation<RemoveClubPost, RemoveClubPostVariables | {}>(
    REMOVE_CLUB_POST,
    {
      variables: {},
      refetchQueries: [
        {
          query: GET_CLUB_POSTS,
          variables: { name: clubName }
        }
      ]
    }
  );
}

export function useClubMembers(clubName: string) {
  return useQuery<GetClubMembers, GetClubMembersVariables | {}>(
    GET_CLUB_MEMBERS,
    { clubName }
  );
}

export function useAcceptSubscription(clubName: string) {
  return useMutation<
    AcceptClubSubscripton,
    AcceptClubSubscriptonVariables | {}
  >(ACCEPT_SUBSCRIPTION, {
    variables: {},
    refetchQueries: [
      {
        query: GET_CLUB_MEMBERS,
        variables: { clubName }
      }
    ]
  });
}

export function useDeclineSubscription(clubName: string) {
  return useMutation<
    DeclineClubSubscripton,
    DeclineClubSubscriptonVariables | {}
  >(DECLINE_SUBSCRIPTION, {
    variables: {},
    refetchQueries: [
      {
        query: GET_CLUB_MEMBERS,
        variables: { clubName }
      }
    ]
  });
}

export function useClubGroup(groupId: string) {
  return useQuery<GetClubGroup, GetClubGroupVariables | {}>(GET_CLUB_GROUP, {
    groupId
  });
}

export function useClubGroups(clubName: string) {
  return useQuery<GetClubGroups, GetClubGroupsVariables | {}>(GET_CLUB_GROUPS, {
    clubName
  });
}

export function useCreateGroup(clubName: string) {
  return useMutation<CreateGroup, CreateGroupVariables | {}>(
    CREATE_CLUB_GROUP,
    {
      variables: {},
      refetchQueries: [
        {
          query: GET_CLUB_GROUPS,
          variables: { clubName }
        }
      ]
    }
  );
}

export function useEditGroup(clubName: string) {
  return useMutation<EditGroup, EditGroupVariables | {}>(EDIT_CLUB_GROUP, {
    variables: {},
    refetchQueries: [
      {
        query: GET_CLUB_GROUPS,
        variables: { clubName }
      }
    ]
  });
}

export function useClubGroupMembers(groupId: string) {
  return useQuery<GetClubGroupMembers, GetClubGroupMembersVariables>(
    GET_CLUB_GROUP_MEMBERS,
    { groupId }
  );
}

export function useAddMemberToGroup(clubName: string, groupId: string) {
  return useMutation<AddMemberToGroup, AddMemberToGroupVariables | {}>(
    ADD_MEMBER_TO_GROUP,
    {
      variables: {},
      refetchQueries: [
        {
          query: GET_CLUB_GROUP_MEMBERS,
          variables: { groupId }
        },
        {
          query: GET_CLUB_MEMBERS,
          variables: { clubName }
        }
      ]
    }
  );
}

export function useRemoveMemberFromGroup(clubName: string, groupId: string) {
  return useMutation<
    RemoveMemberFromGroup,
    RemoveMemberFromGroupVariables | {}
  >(REMOVE_MEMBER_FROM_GROUP, {
    variables: {},
    refetchQueries: [
      {
        query: GET_CLUB_GROUP_MEMBERS,
        variables: { groupId }
      },
      {
        query: GET_CLUB_MEMBERS,
        variables: { clubName }
      }
    ]
  });
}
