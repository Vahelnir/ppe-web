import { defineComponent } from "@vue/composition-api";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { useQuery, useResult } from "@vue/apollo-composable";
import { RawLocation, VueRouter } from "vue-router/types/router";
import GET_CLUBS from "@/graphql/getClubs.graphql";

function buildLinkToFunction(router: VueRouter) {
  return (route: RawLocation) => {
    router.push(route).catch(err => {});
  };
}

export default defineComponent({
  setup(props, ctx) {
    const linkTo = buildLinkToFunction(ctx.root.$router);
    const { result } = useQuery(GET_CLUBS);
    const clubs = useResult(result, [], data => data.clubs);
    return {
      clubs,
      toClub: (club: any) => {
        linkTo({ name: "club", params: { clubName: club.name } });
      },
      faArrowRight
    };
  }
});
