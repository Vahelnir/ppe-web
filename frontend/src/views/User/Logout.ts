import Vue from "vue";
import { defineComponent, watch } from "@vue/composition-api";
import { useQuery, useResult } from "@vue/apollo-composable";
import GET_LOGGED_USER from "@/graphql/getLoggedUser.graphql";
import { disconnect } from "@/service/auth";

export default defineComponent({
  setup(props, ctx) {
    const { $router } = ctx.root;
    const { result, onResult, refetch } = useQuery(GET_LOGGED_USER);
    let alreadyCalled = false;
    const user = useResult(result, null, data => data.me);
    onResult(async data => {
      if (alreadyCalled) return;
      alreadyCalled = true;
      if (data && data.data) {
        await disconnect();
        await refetch();
      }
      Vue.nextTick(() => {
        $router.push({ name: "home" }).catch(_err => {});
      });
    });
  }
});
