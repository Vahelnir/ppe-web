import { defineComponent, Ref, ref } from "@vue/composition-api";
import GET_LOGGED_USER from "@/graphql/getLoggedUser.graphql";
import { useQuery, useResult } from "@vue/apollo-composable";
import { login } from "@/service/auth";
import { VueRouter } from "vue-router/types/router";

function useGetLoggedUser() {
  const { result, onResult, refetch, loading: loadingUser } = useQuery(
    GET_LOGGED_USER
  );
  return { result, onResult, refetch, loadingUser };
}

export default defineComponent({
  setup(props, ctx) {
    const { $router } = ctx.root;
    const email = ref("");
    const password = ref("");
    const errors: Ref<Array<string | null>> = ref([]);

    const { refetch } = useGetLoggedUser();

    return {
      email,
      password,
      errors,
      async connect() {
        errors.value = [];
        const response = await login(email.value, password.value);
        const responseJson = await response.json();
        if (!response.ok) {
          errors.value.push(responseJson.error.text);
          return;
        }
        await refetch();
        $router.push({ name: "home" }).catch(_err => {});
      }
    };
  }
});
