import { defineComponent, ref, Ref } from "@vue/composition-api";
import { login, register } from "@/service/auth";

export default defineComponent({
  setup(props, ctx) {
    const { $router } = ctx.root;
    const errors: Ref<Array<string | null>> = ref([]);
    const email = ref("");
    const password = ref("");
    const passwordVerification = ref("");
    return {
      email,
      password,
      passwordVerification,
      errors,
      async register() {
        errors.value = [];
        const response = await register(
          email.value,
          password.value,
          passwordVerification.value
        );
        const responseJson = await response.json();
        if (!response.ok) {
          errors.value.push(responseJson.error.text);
          return;
        }
        $router.push({ name: "login" }).catch(_err => {});
      }
    };
  }
});
