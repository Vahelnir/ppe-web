import { defineComponent } from "@vue/composition-api";
import { useClubGroups } from "@/hooks/useClub";
import { useResult } from "@vue/apollo-composable";
import { GetClubGroups_club_groups } from "@/graphql/club/types/GetClubGroups";
import { faEdit } from "@fortawesome/free-solid-svg-icons";

function sortAlphabet(
  a: GetClubGroups_club_groups,
  b: GetClubGroups_club_groups
) {
  return a.label.localeCompare(b.label);
}

export default defineComponent({
  props: {
    clubName: {
      required: true,
      type: String
    }
  },
  setup(props) {
    const { result } = useClubGroups(props.clubName);
    const groups = useResult(result, [], data =>
      data.club?.groups?.sort(sortAlphabet)
    );
    return { groups, faEdit };
  }
});
