import { computed, defineComponent, ref } from "@vue/composition-api";
import {
  useAcceptSubscription,
  useClubMembers,
  useDeclineSubscription,
} from "@/hooks/useClub";
import { useMutation, useResult } from "@vue/apollo-composable";
import { faCheck, faBan } from "@fortawesome/free-solid-svg-icons";
import { SubscriptionStatus } from "@/graphql/types/globalTypes";
import ACCEPT_SUBSCRIPTION from "@/graphql/club/acceptClubSubscription.graphql";
import DECLINE_SUBSCRIPTION from "@/graphql/club/declineClubSubscription.graphql";
import {
  AcceptClubSubscripton,
  AcceptClubSubscriptonVariables,
} from "@/graphql/club/types/AcceptClubSubscripton";
import {
  DeclineClubSubscripton,
  DeclineClubSubscriptonVariables,
} from "@/graphql/club/types/DeclineClubSubscripton";
import { useSelectedProfilePermissions } from "@/hooks/useSelectedProfilePermissions";

export default defineComponent({
  props: {
    clubName: {
      type: String,
      required: true,
    },
  },
  setup(props) {
    const list = ref("accepted");
    const { mutate: acceptSubscription } = useAcceptSubscription(
      props.clubName
    );
    const { mutate: declineSubscription } = useDeclineSubscription(
      props.clubName
    );

    const { result } = useClubMembers(props.clubName);
    const members = useResult(result, [], (data) => data.club?.members);

    const pendingMembers = computed(() => {
      return members.value?.filter(
        (member) => member.status.status === SubscriptionStatus.PENDING
      );
    });
    const subscribedMembers = computed(() => {
      return members.value?.filter(
        (member) => member.status.status === SubscriptionStatus.SUBSCRIBED
      );
    });
    const selectedMembers = computed(() => {
      if (list.value === "accepted") {
        return subscribedMembers.value;
      } else {
        return pendingMembers.value;
      }
    });

    const { hasPermission } = useSelectedProfilePermissions(props.clubName);
    return {
      hasPermission,
      members,
      selectedMembers,
      acceptSubscription,
      declineSubscription,
      list,
      faCheck,
      faBan,
    };
  },
});
