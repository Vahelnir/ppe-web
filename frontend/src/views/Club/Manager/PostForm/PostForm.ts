import { defineComponent, ref, SetupContext } from "@vue/composition-api";
import {
  useClubPost,
  useCreateClubPost,
  useEditClubPost
} from "@/hooks/useClub";

function editPostComponent(
  clubName: string,
  postId: string,
  ctx: SetupContext
) {
  const { onResult } = useClubPost(postId);
  const { mutate } = useEditClubPost(clubName);

  const { $router } = ctx.root;
  const title = ref("");
  const image = ref("");
  const content = ref("");
  onResult(result => {
    if (!result || !result.data.post) {
      return;
    }
    const post = result.data.post;
    title.value = post.title;
    image.value = post.image;
    content.value = post.content;
  });
  return {
    title,
    image,
    content,
    async submit() {
      await mutate({
        id: postId,
        postInput: {
          title: title.value,
          content: content.value,
          image: image.value
        }
      });
      $router.push({ name: "club.manager.posts" }).catch(err => {});
    }
  };
}

function createPostComponent(clubName: string, ctx: SetupContext) {
  const { mutate } = useCreateClubPost(clubName);
  const { $router } = ctx.root;

  const title = ref("");
  const image = ref("");
  const content = ref("");
  return {
    title,
    image,
    content,
    async submit() {
      await mutate({
        clubName,
        postInput: {
          title: title.value,
          image: image.value,
          content: content.value
        }
      });
      $router.push({ name: "club.manager.posts" }).catch(err => {});
    }
  };
}

export default defineComponent({
  props: {
    create: {
      required: true,
      type: Boolean
    },
    clubName: {
      required: true,
      type: String
    },
    postId: String
  },
  setup(props, ctx) {
    if (!props.create && props.postId) {
      return editPostComponent(props.clubName, props.postId, ctx);
    } else {
      return createPostComponent(props.clubName, ctx);
    }
  }
});
