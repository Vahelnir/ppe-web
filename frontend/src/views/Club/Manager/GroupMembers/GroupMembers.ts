import { computed, defineComponent, ref } from "@vue/composition-api";
import {
  useAddMemberToGroup,
  useClubGroupMembers,
  useClubMembers,
  useRemoveMemberFromGroup
} from "@/hooks/useClub";
import { useMutation, useResult } from "@vue/apollo-composable";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { GetClubMembers_club_members } from "@/graphql/club/types/GetClubMembers";

function useGroupMembersState(groupId: string) {
  const { result: groupMembersResult } = useClubGroupMembers(groupId);
  const groupMembers = useResult(
    groupMembersResult,
    [],
    data => data.group?.members
  );
  const permissions = useResult(
    groupMembersResult,
    [] as string[],
    data => data.group?.permissions
  );
  const canRemoveMember = computed(
    () =>
      !permissions.value ||
      permissions.value.length === 0 ||
      !permissions.value.includes("*")
  );
  return {
    groupMembers,
    canRemoveMember
  };
}

function useClubMembersState(clubName: string, groupId: string) {
  const { result: clubMembersResult } = useClubMembers(clubName);
  const members = useResult(
    clubMembersResult,
    [] as GetClubMembers_club_members[],
    data => data.club?.members
  );
  const addMemberOption = computed(() => {
    if (members.value) {
      return members.value
        .filter(member => !member.groups.some(group => group.id === groupId))
        .map(member => ({
          label: `${member.profile.name} ${member.profile.surname}`,
          id: member.id
        }));
    }
    return [];
  });
  return { addMemberOption };
}

export default defineComponent({
  props: {
    clubName: {
      required: true,
      type: String
    },
    groupId: {
      required: true,
      type: String
    }
  },
  setup({ groupId, clubName }) {
    const { groupMembers, canRemoveMember } = useGroupMembersState(groupId);
    const { addMemberOption } = useClubMembersState(clubName, groupId);
    const { mutate: addMember } = useAddMemberToGroup(clubName, groupId);
    const { mutate: removeMember } = useRemoveMemberFromGroup(
      clubName,
      groupId
    );
    const selectedMemberId = ref("");
    return {
      groupMembers,
      canRemoveMember,
      addMemberOption,
      selectedMemberId,
      faTimes,
      async remove(memberId: string) {
        const res = await removeMember({
          groupId,
          memberId: memberId
        });
      },
      async add() {
        if (selectedMemberId.value) {
          const res = await addMember({
            groupId,
            memberId: selectedMemberId.value
          });
          if (!res.errors) {
            selectedMemberId.value = "";
          }
        }
      }
    };
  }
});
