import { defineComponent } from "@vue/composition-api";
import { useResult } from "@vue/apollo-composable";
import { faEdit, faTimes } from "@fortawesome/free-solid-svg-icons";
import { useClubPosts, useRemoveClubPost } from "@/hooks/useClub";
import { useSelectedProfilePermissions } from "@/hooks/useSelectedProfilePermissions";

export default defineComponent({
  props: {
    clubName: {
      type: String,
      required: true
    }
  },
  setup(props) {
    const { result } = useClubPosts(props.clubName);
    const posts = useResult(result, [], data => data.club?.posts);
    const { mutate: deletePost } = useRemoveClubPost(props.clubName);

    const { hasPermission } = useSelectedProfilePermissions(props.clubName);
    return {
      hasPermission,
      posts,
      getDateTime(timestamp: string) {
        const date = new Date(+timestamp);
        return `${date.toLocaleDateString()} à ${date.toLocaleTimeString()}`;
      },
      async removePost(postId: string) {
        const answer = confirm(
          "Êtes-vous sur de vouloir supprimer cet article ?"
        );
        if (answer) {
          await deletePost({ postId });
        }
      },
      faEdit,
      faTimes
    };
  }
});
