import { defineComponent, ref, SetupContext } from "@vue/composition-api";
import { useClubGroup, useCreateGroup, useEditGroup } from "@/hooks/useClub";

function useCreateComponent(clubName: string, ctx: SetupContext) {
  const { $router } = ctx.root;
  const { mutate: createGroup } = useCreateGroup(clubName);
  const name = ref("");
  const permissions = ref([]);
  return {
    name,
    permissions,
    async submit() {
      await createGroup({
        clubName,
        group: { label: name.value, permissions: permissions.value }
      });
      $router.push({ name: "club.manager.groups" }).catch((_err: any) => {});
    }
  };
}

function useEditComponent(
  clubName: string,
  groupId: string,
  ctx: SetupContext
) {
  const { $router } = ctx.root;
  const { onResult } = useClubGroup(groupId);
  const { mutate: editGroup } = useEditGroup(clubName);
  const name = ref("");
  const permissions = ref<string[]>([]);
  onResult(data => {
    const group = data?.data.group;
    if (group) {
      name.value = group.label;
      permissions.value = group.permissions;
    }
  });
  return {
    name,
    permissions,
    async submit() {
      await editGroup({
        groupId,
        group: { label: name.value, permissions: permissions.value }
      });
      $router.push({ name: "club.manager.groups" }).catch((_err: any) => {});
    }
  };
}

export default defineComponent({
  props: {
    create: {
      required: true,
      type: Boolean
    },
    clubName: {
      required: true,
      type: String
    },
    groupId: String
  },
  setup(props, ctx) {
    if (!props.create && props.groupId) {
      return useEditComponent(props.clubName, props.groupId, ctx);
    } else {
      return useCreateComponent(props.clubName, ctx);
    }
  }
});
