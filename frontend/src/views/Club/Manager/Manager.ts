import { defineComponent } from "@vue/composition-api";
import { useSelectedProfilePermissions } from "@/hooks/useSelectedProfilePermissions";

export default defineComponent({
  props: {
    clubName: {
      type: String,
      required: true
    }
  },
  setup({ clubName }) {
    const { permissions } = useSelectedProfilePermissions(clubName);
    return { permissions };
  }
});
