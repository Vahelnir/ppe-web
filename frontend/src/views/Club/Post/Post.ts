import { defineComponent } from "@vue/composition-api";
import { useQuery, useResult } from "@vue/apollo-composable";
import GET_CLUB_POST from "@/graphql/club/getClubPost.graphql";

export default defineComponent({
  props: {
    clubName: String,
    id: String
  },
  setup(props, attr) {
    const postId = props.id;
    const { result } = useQuery(GET_CLUB_POST, {
      id: postId
    });
    const post = useResult(result, null, data => data.post);
    return { post };
  }
});
