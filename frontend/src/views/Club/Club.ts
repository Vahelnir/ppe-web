import { defineComponent } from "@vue/composition-api";
import { useQuery, useResult } from "@vue/apollo-composable";
import ClubSubscribeWidget from "@/components/Club/ClubSubscribeWidget.vue";
import GET_CLUB from "@/graphql/club/getClub.graphql";
import GET_LOGGED_MEMBER_PERMISSIONS_FOR_CLUB from "@/graphql/getLoggedMemberPermissionsForClub.graphql";
import {
  GetLoggedMemberPermissionsForClub,
  GetLoggedMemberPermissionsForClubVariables
} from "@/graphql/types/GetLoggedMemberPermissionsForClub";
import { useSelectedProfilePermissions } from "@/hooks/useSelectedProfilePermissions";

export default defineComponent({
  components: {
    ClubSubscribeWidget
  },
  props: {
    clubName: {
      type: String,
      required: true
    }
  },
  setup(props, ctx) {
    const { clubName } = props;
    const { result } = useQuery(GET_CLUB, { name: clubName });
    const club = useResult(result, { name: "loading..." }, data => data.club);

    const { permissions } = useSelectedProfilePermissions(clubName);
    return {
      permissions,
      club
    };
  }
});
