import { defineComponent } from "@vue/composition-api";
import { useQuery, useResult } from "@vue/apollo-composable";
import GET_CLUB_POSTS from "@/graphql/club/getClubPosts.graphql";

export default defineComponent({
  props: {
    clubName: String
  },
  setup(props, ctx) {
    const { clubName } = props;
    const { result } = useQuery(GET_CLUB_POSTS, {
      name: clubName
    });
    const posts = useResult(result, [], data => data.club.posts);
    return {
      posts,
      toPost: (postId: string) => {
        ctx.root.$router
          .push({ name: "club.post", params: { id: postId } })
          .then(_err => {});
      }
    };
  }
});
