import { defineComponent } from "@vue/composition-api";
import { useMutation, useQuery, useResult } from "@vue/apollo-composable";
import { VueRouter } from "vue-router/types/router";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import GET_USER_PROFILES from "@/graphql/getLoggedUserProfiles.graphql";
import SET_ACTIVE_PROFILE from "@/graphql/setActiveProfile.graphql";
import { useLoggedProfile } from "@/hooks/useLoggedUserProfile";

interface SetActiveProfileVariables {
  id?: string;
}
function createActiveProfileMutation($router: VueRouter) {
  const { mutate, onDone, onError } = useMutation<
    any,
    SetActiveProfileVariables
  >(SET_ACTIVE_PROFILE, {
    variables: {}
  });
  return {
    onDone,
    onError,
    async setActiveProfile(id: string) {
      try {
        await mutate({ id }, {});
        await $router.push({ name: "home" });
      } catch (err) {}
    }
  };
}

export default defineComponent({
  setup(props, ctx) {
    const { result } = useQuery(GET_USER_PROFILES);
    const profiles = useResult(result, {}, data => data.me.profiles);
    const { result: selectedProfileResult } = useLoggedProfile();
    const hasProfileSelected = useResult(
      selectedProfileResult,
      false,
      data => !!data.selectedProfile
    );
    const { setActiveProfile } = createActiveProfileMutation(ctx.root.$router);
    return { hasProfileSelected, profiles, setActiveProfile, faEdit };
  }
});
