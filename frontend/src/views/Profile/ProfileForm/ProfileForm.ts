import { defineComponent, Ref, ref } from "@vue/composition-api";
import {
  useUserCreateProfile,
  useUserEditProfile,
  useUserProfile
} from "@/hooks/useLoggedUserProfile";
import { Gender } from "@/graphql/types/globalTypes";
import { VueRouter } from "vue-router/types/router";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

function createProfileState($router: VueRouter) {
  const { mutate } = useUserCreateProfile();
  const errors: Ref<Array<string>> = ref([]);
  const email = ref("");
  const name = ref("");
  const surname = ref("");
  const gender: Ref<Gender> = ref("WOMAN");
  const address = ref("");
  const postcode = ref("");
  const city = ref("");
  const country = ref("");
  return {
    errors,
    email,
    name,
    surname,
    gender,
    address,
    postcode,
    city,
    country,
    async submitForm() {
      errors.value = [];
      try {
        const mutateResult = await mutate(
          {
            profile: {
              email: email.value,
              name: name.value,
              surname: surname.value,
              gender: gender.value,
              address: address.value,
              postcode: postcode.value,
              city: city.value,
              country: country.value
            }
          },
          {}
        );
        if (mutateResult.errors?.length) {
          errors.value = [
            ...errors.value,
            ...mutateResult.errors.map(err => err.message)
          ];
        } else {
          await $router.push({ name: "profile.select" });
        }
      } catch (err) {
        errors.value.push("Une erreur est survenue");
      }
    }
  };
}

function editProfileState($router: any, profileId: string) {
  const { mutate } = useUserEditProfile();
  const { result, onResult, refetch } = useUserProfile({ profileId });
  const errors: Ref<Array<string>> = ref([]);
  const email = ref("");
  const name = ref("");
  const surname = ref("");
  const gender: Ref<Gender> = ref("");
  const address = ref("");
  const postcode = ref("");
  const city = ref("");
  const country = ref("");

  onResult(() => {
    const profile = result.value?.me?.profile;
    if (profile) {
      email.value = profile.email;
      name.value = profile.name;
      surname.value = profile.surname;
      gender.value = profile.gender;
      address.value = profile.address;
      postcode.value = profile.postcode;
      city.value = profile.city;
      country.value = profile.country;
    } else {
      errors.value.push(
        "Une erreur est survenue lors de la récupération du profil."
      );
    }
  });

  return {
    errors,
    email,
    name,
    surname,
    gender,
    address,
    postcode,
    city,
    country,
    async submitForm() {
      errors.value = [];
      try {
        const mutateResult = await mutate(
          {
            id: profileId,
            profile: {
              email: email.value,
              name: name.value,
              surname: surname.value,
              gender: gender.value,
              address: address.value,
              postcode: postcode.value,
              city: city.value,
              country: country.value
            }
          },
          {}
        );
        if (mutateResult.errors?.length) {
          errors.value = [
            ...errors.value,
            ...mutateResult.errors.map(err => err.message)
          ];
        } else {
          await refetch();
          await $router.push({ name: "profile.select" });
        }
      } catch (err) {
        errors.value.push("Une erreur est survenue");
      }
    }
  };
}

export default defineComponent({
  props: {
    action: {
      default: "create",
      type: String,
      validator: (value: string) => ["create", "edit"].indexOf(value) !== -1,
      required: true
    },
    profileId: String
  } as const,
  setup(props, ctx) {
    const { $router } = ctx.root;
    let res;
    if (props.action === "create") {
      res = createProfileState($router);
    } else if (props.profileId) {
      res = editProfileState($router, props.profileId);
    } else {
      /* eslint-disable */
      console.error(
        `An error occured while initializing ProfileForm with action: ${props.action}`
      );
    }
    return Object.assign(
      {
        faArrowLeft
      },
      res
    );
  }
});
