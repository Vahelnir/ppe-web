import {
  faBaseballBall,
  faBasketballBall,
  faBiking,
  faBowlingBall,
  faDumbbell,
  faFootballBall,
  faFutbol,
  faGolfBall,
  faHockeyPuck,
  faQuidditch,
  faRunning,
  faSkating,
  faSkiing,
  faSkiingNordic,
  faSnowboarding,
  faSwimmer,
  faTableTennis,
  faVolleyballBall,
  faGamepad
} from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";

export const sportsIcon = [
  faBaseballBall,
  faBasketballBall,
  faBiking,
  faBowlingBall,
  faDumbbell,
  faFootballBall,
  faFutbol,
  faGolfBall,
  faHockeyPuck,
  faQuidditch,
  faRunning,
  faSkating,
  faSkiing,
  faSkiingNordic,
  faSnowboarding,
  faSwimmer,
  faTableTennis,
  faVolleyballBall,
  faGamepad
];

library.add(...sportsIcon);
