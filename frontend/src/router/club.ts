import { RouteConfig } from "vue-router";
import clubManager from "./clubManager";
import Posts from "@/views/Club/Posts/Posts.vue";
import Post from "@/views/Club/Post/Post.vue";
import Manager from "@/views/Club/Manager/Manager.vue";

const routes: RouteConfig[] = [
  {
    path: "",
    component: Posts,
    name: "club",
    props: true
  },
  {
    path: "post/:id",
    component: Post,
    name: "club.post",
    props: true
  },
  {
    path: "manage",
    component: Manager,
    props: true,
    children: clubManager
  }
];
export default routes;
