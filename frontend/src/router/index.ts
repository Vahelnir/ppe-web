import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import { apolloClient } from "@/vue-apollo";
import auth from "@/router/auth";
import clubs from "@/router/clubs";
import profile from "@/router/profile";
import Home from "@/views/Home/Home.vue";
import About from "@/views/About/About.vue";
import Error from "@/views/PageError/PageError.vue";
import GET_LOGGED_USER from "@/graphql/getLoggedUser.graphql";
import GET_LOGGED_USER_PROFILE from "@/graphql/getLoggedUserProfile.graphql";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/about",
    name: "about",
    component: About
  },
  {
    path: "/error",
    name: "error",
    component: Error
  },
  ...auth,
  ...clubs,
  ...profile
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {
  let userResult;
  let profileResult;
  try {
    userResult = await apolloClient.query({
      query: GET_LOGGED_USER,
      fetchPolicy: "network-only"
    });
    profileResult = await apolloClient.query({
      query: GET_LOGGED_USER_PROFILE,
      fetchPolicy: "network-only"
    });
  } catch (err) {
    return next({
      name: "error",
      params: { error: err }
    });
  }
  let isLogged = false;
  let hasAnyProfiles = false;
  let hasSelectedProfile = false;

  if (userResult.data && userResult.data.me) isLogged = true;
  if (
    isLogged &&
    userResult.data.me.profiles &&
    userResult.data.me.profiles.length > 0
  ) {
    hasAnyProfiles = true;
  }

  if (profileResult.data && profileResult.data.selectedProfile) {
    hasSelectedProfile = true;
  }

  if (to.meta.shouldBeLogged && !isLogged) {
    return next({ name: "login" });
  } else if (to.meta?.shouldBeLogged === false && isLogged) {
    return next({ name: "home" });
  }

  if (
    isLogged &&
    !hasAnyProfiles &&
    to.name !== "profile.create" &&
    to.name !== "logout"
  ) {
    return next({ name: "profile.create" });
  }

  if (
    isLogged &&
    !hasSelectedProfile &&
    to.name !== "profile.select" &&
    to.name !== "profile.create" &&
    to.name !== "logout"
  ) {
    return next({ name: "profile.select" });
  }
  return next();
});

export default router;
