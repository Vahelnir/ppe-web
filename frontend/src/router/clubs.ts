import { RouteConfig } from "vue-router";
import Clubs from "@/views/Clubs/Clubs.vue";
import Club from "@/views/Club/Club.vue";
import association from "@/router/club";

const routes: RouteConfig[] = [
  {
    path: "/associations",
    name: "clubs",
    component: Clubs
  },
  {
    path: "/associations/:clubName",
    component: Club,
    props: true,
    children: association
  }
];
export default routes;
