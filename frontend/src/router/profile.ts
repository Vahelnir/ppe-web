import { RouteConfig } from "vue-router";
import ProfileSelect from "@/views/Profile/ProfileSelect/ProfileSelect.vue";
import ProfileCreate from "@/views/Profile/ProfileForm/ProfileForm.vue";

const routes: RouteConfig[] = [
  {
    path: "/profile/select",
    name: "profile.select",
    component: ProfileSelect,
    meta: {
      shouldBeLogged: true
    }
  },
  {
    path: "/profile/create",
    name: "profile.create",
    component: ProfileCreate,
    props: { action: "create" },
    meta: {
      shouldBeLogged: true
    }
  },
  {
    path: "/profile/edit/:profileId",
    name: "profile.edit",
    component: ProfileCreate,
    props: ({ params: { profileId } }) => ({ action: "edit", profileId }),
    meta: {
      shouldBeLogged: true
    }
  }
];
export default routes;
