import { RouteConfig } from "vue-router";
import Login from "@/views/User/Login.vue";
import Logout from "@/views/User/Logout.vue";
import Register from "@/views/User/Register.vue";

const routes: RouteConfig[] = [
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: {
      shouldBeLogged: false
    }
  },
  {
    path: "/logout",
    name: "logout",
    component: Logout,
    meta: {
      shouldBeLogged: true
    }
  },
  {
    path: "/register",
    name: "register",
    component: Register,
    meta: {
      shouldBeLogged: false
    }
  }
];
export default routes;
