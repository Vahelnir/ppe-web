import { RouteConfig } from "vue-router";
import Posts from "@/views/Club/Manager/Posts/Posts.vue";
import PostForm from "@/views/Club/Manager/PostForm/PostForm.vue";
import Members from "@/views/Club/Manager/Members/Members.vue";
import Groups from "@/views/Club/Manager/Groups/Groups.vue";
import GroupForm from "@/views/Club/Manager/GroupForm/GroupForm.vue";
import GroupMembers from "@/views/Club/Manager/GroupMembers/GroupMembers.vue";

const routes: RouteConfig[] = [
  {
    path: "",
    name: "club.manager",
    redirect: { name: "club.manager.posts" }
  },
  {
    path: "posts",
    component: Posts,
    name: "club.manager.posts",
    props: true
  },
  {
    path: "posts/edit/:postId",
    component: PostForm,
    name: "club.manager.post.edit",
    props: ({ params: { clubName, postId } }) => ({
      clubName,
      postId,
      create: false
    })
  },
  {
    path: "posts/create",
    component: PostForm,
    name: "club.manager.post.create",
    props: ({ params: { clubName } }) => ({ clubName, create: true })
  },
  {
    path: "members",
    component: Members,
    name: "club.manager.members",
    props: true
  },
  {
    path: "groups",
    component: Groups,
    name: "club.manager.groups",
    props: true
  },
  {
    path: "group/:groupId/members",
    component: GroupMembers,
    name: "club.manager.group.members",
    props: true
  },
  {
    path: "groups/:groupId/edit",
    component: GroupForm,
    name: "club.manager.group.edit",
    props: ({ params: { clubName, groupId } }) => ({
      clubName,
      groupId,
      create: false
    })
  },
  {
    path: "groups/create",
    component: GroupForm,
    name: "club.manager.group.create",
    props: ({ params: { clubName } }) => ({ clubName, create: true })
  }
];

export default routes;
