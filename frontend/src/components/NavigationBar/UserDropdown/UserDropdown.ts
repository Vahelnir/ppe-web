import { defineComponent, ref } from "@vue/composition-api";
import GET_LOGGED_USER from "@/graphql/getLoggedUser.graphql";
import { useQuery, useResult } from "@vue/apollo-composable";
import { useLoggedProfile } from "@/hooks/useLoggedUserProfile";
import { faCaretUp, faCaretDown } from "@fortawesome/free-solid-svg-icons";

export default defineComponent({
  setup() {
    const { result: userResult } = useQuery(GET_LOGGED_USER);
    const { result: profileResult } = useLoggedProfile();
    const user = useResult(userResult, null, data => data.me);
    const profile = useResult(
      profileResult,
      null,
      data => data.selectedProfile
    );

    const opened = ref(false);
    function onClickOutside() {
      opened.value = false;
    }

    return { user, profile, opened, onClickOutside, faCaretUp, faCaretDown };
  }
});
