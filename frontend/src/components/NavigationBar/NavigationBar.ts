import { defineComponent, ref, computed } from "@vue/composition-api";
import { useQuery, useResult } from "@vue/apollo-composable";
import UserDropdown from "./UserDropdown/UserDropdown.vue";

export default defineComponent({
  name: "navigation-bar",
  components: {
    UserDropdown
  },
  setup() {
    const opened = ref(false);
    return { opened };
  }
});
