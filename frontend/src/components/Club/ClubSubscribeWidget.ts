import { computed, defineComponent } from "@vue/composition-api";
import {
  useGlobalQueryLoading,
  useMutation,
  useQuery,
  useResult
} from "@vue/apollo-composable";
import GET_LOGGED_MEMBER_SUBSCRIPTIONS_FOR_CLUB from "@/graphql/getLoggedMemberSubscriptionsForClub.graphql";
import SUBSCRIBE_TO_CLUB from "@/graphql/club/subscribeToClub.graphql";
import {
  SubscribeToClub,
  SubscribeToClubVariables
} from "@/graphql/club/types/SubscribeToClub";

function getSubscriptionInfo(clubName: string) {
  const {
    result: subscriptionsResult,
    onResult,
    onError
  } = useQuery(GET_LOGGED_MEMBER_SUBSCRIPTIONS_FOR_CLUB, { clubName });
  const profile = useResult(
    subscriptionsResult,
    null,
    data => data.selectedProfile
  );
  const status = useResult(
    subscriptionsResult,
    "loading",
    data => data.selectedProfile.subscribedTo.status
  );
  const subscription = useResult(
    subscriptionsResult,
    "loading",
    data => data.selectedProfile.subscribedTo.subscription
  );
  return { profile, status, subscription };
}
function getSubscribeMutation() {
  return useMutation<SubscribeToClub, SubscribeToClubVariables | {}>(
    SUBSCRIBE_TO_CLUB,
    { variables: {} }
  );
}

export default defineComponent({
  props: {
    clubName: String
  },
  setup({ clubName }, ctx) {
    if (!clubName) {
      return ctx.root.$router
        .push({
          name: "error",
          params: { error: "[ClubSubscribeWidget] no clubname" }
        })
        .catch(_err => {});
    }
    const { profile, subscription, status } = getSubscriptionInfo(clubName);
    const { mutate: subscribe } = getSubscribeMutation();
    return {
      profile,
      subscription,
      status,
      subscribeAction() {
        subscribe(
          { clubName },
          {
            refetchQueries: [
              {
                query: GET_LOGGED_MEMBER_SUBSCRIPTIONS_FOR_CLUB,
                variables: { clubName }
              }
            ]
          }
        ).catch(_err => {});
      },
      loading: useGlobalQueryLoading()
    };
  }
});
